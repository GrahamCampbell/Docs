<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
	const PDF = 0;
	const ERATTA = 1;

    /**
     * Get the publication we belong to.
     */
    public function publication()
    {
        return $this->belongsTo(Publication::class);
    }

    public function filePath()
    {
    	return storage_path("app/local/{$this->id}.pdf");
    }
}
