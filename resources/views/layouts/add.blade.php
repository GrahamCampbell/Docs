<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
  <title>Publication Manager</title>
</head>
<body class="antialiased">
  @yield('content')
</div>
<script src="{{ mix('/js/app.js') }}"></script>
<script>
  var should_refresh_citation_index = true;

  function loadCitationIndex(authors, year) {
    jQuery.post('/citation', {'authors': authors, 'year': year}, function (data) {
      $("#citation_index").val(data.citation_index);
    });
  }

  $(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#authors').on('change keyup paste', function(e) {
        if (should_refresh_citation_index) {
          loadCitationIndex($('#authors').val(), $('#year').val());
        }
    });
    $('#year').on('change keyup paste', function(e) {
        if (should_refresh_citation_index) {
          loadCitationIndex($('#authors').val(), $('#year').val());
        }
    });
    $('#citation_index').on('keyup', function(e) {
        should_refresh_citation_index = false;
    });
  });
</script>
</body>
</html>
