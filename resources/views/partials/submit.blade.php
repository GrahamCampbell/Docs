<div class="flex flex-wrap -mx-3 mt-12">
  <div class="w-full md:w-100 px-3">
    <input type="submit" class="w-full text-lg bg-purple-500 hover:bg-purple-400 text-white font-bold py-3 px-4 focus:outline-none focus:shadow-outline" value="SUBMIT" name="submit">
  </div>
</div>
