<div class="flex flex-wrap -mx-3 my-8">
  <div class="w-full md:w-1/3 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="journal">
      Journal
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="journal" name="journal" type="text" placeholder="Journal Name">
  </div>
  <div class="w-full md:w-1/3 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="publisher">
      Publisher
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="publisher" name="publisher" type="text" placeholder="ACME Publishing">
  </div>
  <div class="w-full md:w-1/6 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="startpage">
      First Page
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="startpage" name="startpage" type="text" placeholder="27">
  </div>
  <div class="w-full md:w-1/6 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="endpage">
      Last Page
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="endpage" name="endpage" type="text" placeholder="64">
  </div>
</div>
