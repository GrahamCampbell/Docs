<div class="flex flex-wrap -mx-3 my-8">
  <div class="w-full md:w-5/12 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="eprint">
      E-Print ID
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="eprint" name="eprint" type="text" placeholder="1204.0299">
  </div>
  <div class="w-full md:w-7/12 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="clazz">
      Primary Class
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="clazz" name="clazz" type="text" placeholder="math.LO">
  </div>
</div>
