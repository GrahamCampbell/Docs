<?php

namespace App\Tex;

class Bibfile
{
    public static function encode(string $type, string $index, array $data)
    {
        return sprintf('@%s{%s}', $type, self::body($index, $data));
    }

    private static function body(string $index, array $data)
    {
        $padding = max(array_map('strlen', array_keys($data)));

        $output = "{$index},\n";

        foreach ($data as $key => $value) {
            $output .= sprintf("  %-{$padding}s = ", $key).sprintf('{%s},', $value)."\n";
        }

        return $output;
    }
}
