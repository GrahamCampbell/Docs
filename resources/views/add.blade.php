@extends('layouts.default')

@section('content')
<div class="relative max-w-6xl mx-auto">
  <div class="min-h-screen lg:flex-row lg:items-center lg:p-8">
    <h1 class="pb-4">Add Publication</h1>
    <h3 class="pb-8">Please select the publication category.</h3>
    <div class="flex mb-4">
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="BOOK" onclick="location.href='/add/0';">
      </div>
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="IN BOOK" onclick="location.href='/add/1';">
      </div>
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="IN PROCEEDINGS" onclick="location.href='/add/2';">
      </div>
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="ARTICLE" onclick="location.href='/add/3';">
      </div>
    </div>
    <div class="flex mb-4">
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="BSC THESIS" onclick="location.href='/add/7';">
      </div>
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="MENG THESIS" onclick="location.href='/add/11';">
      </div>
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="PHD THESIS" onclick="location.href='/add/5';">
      </div>
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="HABILITATION THESIS" onclick="location.href='/add/6';">
      </div>
    </div>
    <div class="flex mb-4">
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="LECTURE NOTES" onclick="location.href='/add/8';">
      </div>
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="LECTURES SLIDES" onclick="location.href='/add/9';">
      </div>
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="TALK SLIDES" onclick="location.href='/add/13';">
      </div>
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="ARXIV" onclick="location.href='/add/12';">
      </div>
    </div>
    <div class="flex mb-4">
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="PREPRINT" onclick="location.href='/add/10';">
      </div>
      <div class="w-1/4 h-12 ml-1 mr-1 mt-1 mb-1">
        <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3" value="TECHNICAL REPORT" onclick="location.href='/add/4';">
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ mix('/js/app.js') }}"></script>
@endsection
