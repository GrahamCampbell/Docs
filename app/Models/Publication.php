<?php

namespace App\Models;

use App\Tex\Bibfile;
use App\Tex\Encoder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;

class Publication extends Model
{
    use Searchable;

    const BOOK = 0;
    const IN_BOOK = 1;
    const IN_PROCEEDINGS = 2;
    const ARTICLE = 3;
    const TECH_REPORT = 4;
    const PHD_THESIS = 5;
    const HAB_THESIS = 6;
    const BSC_THESIS = 7;
    const LECTURE_NOTES = 8;
    const LECTURE_SLIDES = 9;
    const PREPRINT = 10;
    const MENG_THESIS = 11;
    const ARXIV = 12;
    const TALK_SLIDES = 13;

    const BIB_ORDER = [
      'author',
      'editor',
      'title',
      'booktitle',
      'journal',
      'type',
      'institution',
      'school',
      'publisher',
      'series',
      'volume',
      'number',
      'pages',
      'year',
      'eprint',
      'archivePrefix',
      'primaryClass',
      'note',
      'doi',
      'url',
    ];

    /**
     * Get all of the attached files.
     */
    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    public static function createBook(string $index, string $title, string $authors, string $editors, string $year, string $publisher, string $edition = null, string $series = null, string $volume = null, string $doi = null)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'edition'        => $edition,
            'authors'        => $authors,
            'editors'        => $editors,
            'publisher'      => $publisher,
            'series'         => $series,
            'volume'         => $volume,
            'year'           => $year,
            'doi'            => $doi,
        ]), ['classification' => self::BOOK]));
    }

    public static function createInBook(string $index, string $title, string $booktitle, string $authors, string $editors, string $startpage, string $endpage, string $year, string $publisher, string $series = null, string $volume = null, string $doi = null)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'booktitle'      => $booktitle,
            'authors'        => $authors,
            'editors'        => $editors,
            'publisher'      => $publisher,
            'series'         => $series,
            'volume'         => $volume,
            'start_page'     => $startpage,
            'end_page'       => $endpage,
            'year'           => $year,
            'doi'            => $doi,
        ]), ['classification' => self::IN_BOOK]));
    }

    public static function createInProceedings(string $index, string $title, string $booktitle, string $authors, string $editors, string $startpage, string $endpage, string $year, string $publisher, string $series = null, string $volume = null, string $doi = null)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'booktitle'      => $booktitle,
            'authors'        => $authors,
            'editors'        => $editors,
            'publisher'      => $publisher,
            'series'         => $series,
            'volume'         => $volume,
            'start_page'     => $startpage,
            'end_page'       => $endpage,
            'year'           => $year,
            'doi'            => $doi,
        ]), ['classification' => self::IN_PROCEEDINGS]));
    }

    public static function createArticle(string $index, string $title, string $journal, string $authors, string $startpage, string $endpage, string $year, string $publisher, string $volume, string $number = null, string $doi = null)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'booktitle'      => $journal,
            'authors'        => $authors,
            'publisher'      => $publisher,
            'volume'         => $number ? "{$volume}/{$number}" : $volume,
            'start_page'     => $startpage,
            'end_page'       => $endpage,
            'year'           => $year,
            'doi'            => $doi,
        ]), ['classification' => self::ARTICLE]));
    }

    public static function createTechReport(string $index, string $title, string $authors, string $institution, string $year, string $url = null)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'authors'        => $authors,
            'institution'    => $institution,
            'year'           => $year,
            'url'            => $url,
        ]), ['classification' => self::TECH_REPORT]));
    }

    public static function createPhdThesis(string $index, string $title, string $author, string $school, string $year, string $url = null)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'authors'        => $author,
            'institution'    => $school,
            'year'           => $year,
            'url'            => $url,
        ]), ['classification' => self::PHD_THESIS]));
    }

    public static function createHabThesis(string $index, string $title, string $author, string $school, string $year, string $url = null)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'authors'        => $author,
            'institution'    => $school,
            'year'           => $year,
            'url'            => $url,
        ]), ['classification' => self::HAB_THESIS]));
    }

    public static function createBscThesis(string $index, string $title, string $author, string $school, string $year, string $url = null)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'authors'        => $author,
            'institution'    => $school,
            'year'           => $year,
            'url'            => $url,
        ]), ['classification' => self::BSC_THESIS]));
    }

    public static function createMEngThesis(string $index, string $title, string $author, string $school, string $year, string $url = null)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'authors'        => $author,
            'institution'    => $school,
            'year'           => $year,
            'url'            => $url,
        ]), ['classification' => self::MENG_THESIS]));
    }

    public static function createLectureNotes(string $index, string $title, string $authors, string $institution, string $year, string $url = null)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'authors'        => $authors,
            'institution'    => $institution,
            'year'           => $year,
            'url'            => $url,
        ]), ['classification' => self::LECTURE_NOTES]));
    }

    public static function createLectureSlides(string $index, string $title, string $authors, string $institution, string $year, string $url = null)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'authors'        => $authors,
            'institution'    => $institution,
            'year'           => $year,
            'url'            => $url,
        ]), ['classification' => self::LECTURE_SLIDES]));
    }

    public static function createTalkSlides(string $index, string $title, string $event, string $authors, string $year, string $url = null)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'booktitle'      => $event,
            'authors'        => $authors,
            'year'           => $year,
            'url'            => $url,
        ]), ['classification' => self::TALK_SLIDES]));
    }

    public static function createPreprint(string $index, string $title, string $authors, string $institution, string $year, string $url = null)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'authors'        => $authors,
            'institution'    => $institution,
            'year'           => $year,
            'url'            => $url,
        ]), ['classification' => self::PREPRINT]));
    }

    public static function createArxiv(string $index, string $title, string $authors, string $year, string $eprint, string $clazz)
    {
        return static::forceCreate(array_merge(array_filter([
            'citation_index' => $index,
            'title'          => $title,
            'authors'        => $authors,
            'series'         => $clazz,
            'volume'         => $eprint,
            'year'           => $year,
        ]), ['classification' => self::ARXIV]));
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        if ($this->classification == self::ARXIV) {
            $subtitle = "[{$this->series}] {$this->volume}";
        } else {
            $subtitle = $this->classification == self::ARTICLE
                ? "{$this->booktitle} {$this->volume}"
                : (($this->booktitle ?: $this->institution) ?: $this->publisher);
        }

        return [
            'citation_index'  => $this->citation_index,
            'classification'  => $this->renderClassification(),
            'title'           => $this->renderTitleAndEdition(),
            'subtitle'        => Encoder::encode($subtitle),
            'authors'         => $this->renderAuthors(),
            'raw_authors'     => Encoder::encode($this->authors),
            'publisher'       => Encoder::encode($this->publisher),
            'series'          => Encoder::encode(trim($this->series ? "{$this->series} {$this->volume}" : '')),
            'year'            => $this->year,
            'attached'        => $this->hasPdf() ? 'pdf' : '',
            'link'            => $this->renderLinkUrl() ?? '',
            'proxy'           => $this->renderProxy() ?? '',
            'scholar'         => $this->renderScholar(),
            'dblp'            => $this->renderDblp(),
        ];
    }

    public function hasPdf()
    {
        return $this->attachments()->where('type', Attachment::PDF)->count() > 0;
    }

    public function renderAuthors()
    {
        return collect(explode(' and ', Encoder::encode($this->authors)))->map(function ($author) {
            if (strpos($author, ' ') === false) {
                return $author;
            }

            list($first, $last) = explode(' ', $author, 2);

            return substr(preg_replace('/[^A-Za-z]/', '', Str::ascii($author)), 0, 1).'. '.$last;
        })->join(', ', ', and ');
    }

    public function hasEditors()
    {
        return !empty($this->editors);
    }

    public function renderEditors()
    {
        if (!$this->hasEditors()) {
            return;
        }

        return collect(explode(' and ', Encoder::encode($this->editors)))->map(function ($author) {
            if (strpos($author, ' ') === false) {
                return $author;
            }

            list($first, $last) = explode(' ', $author, 2);

            return substr(preg_replace('/[^A-Za-z]/', '', Str::ascii($author)), 0, 1).'. '.$last;
        })->join(', ', ', and ');
    }

    public function hasDescription()
    {
        return !empty($this->booktitle ?: $this->institution);
    }

    public function renderDescription()
    {
        if (!$this->hasDescription()) {
            return;
        }

        if ($this->booktitle) {
            switch ($this->classification) {
                case self::TALK_SLIDES:
                    return Encoder::encode(sprintf('Presented at the %s', $this->booktitle));
                case self::IN_BOOK:
                case self::IN_PROCEEDINGS:
                    return Encoder::encode(sprintf('In: %s', $this->booktitle));
                default:
                    return Encoder::encode($this->booktitle);
            }
        }

        return Encoder::encode($this->institution);
    }

    public function hasPublisher()
    {
        return !empty($this->publisher);
    }

    public function renderPublisher()
    {
        if (!$this->hasPublisher()) {
            return;
        }

        return Encoder::encode($this->publisher);
    }

    public function hasDoi()
    {
        return !empty($this->doi);
    }

    public function renderDoi()
    {
        if (!$this->hasDoi()) {
            return;
        }

        return "https://doi.org/{$this->doi}";
    }

    public function hasProxy()
    {
        return !empty($this->doi);
    }

    public function renderProxy()
    {
        if (!$this->hasProxy()) {
            return;
        }

        return "https://doi-org.libproxy.ncl.ac.uk/{$this->doi}";
    }

    public function hasArxivUrl()
    {
        return $this->classification == self::ARXIV;
    }

    public function renderArxivUrl()
    {
        if (!$this->hasArxivUrl()) {
            return;
        }

        return "https://arxiv.org/abs/{$this->volume}";
    }

    public function hasLink()
    {
        return $this->hasDoi() || $this->hasArxivUrl() || $this->url;
    }

    public function renderLinkUrl()
    {
        return $this->renderDoi() ?? $this->renderArxivUrl() ?? $this->url ?: null;
    }

    public function renderLinkText()
    {
        if ($this->hasArxivUrl()) {
            return "arXiv:{$this->volume}";
        }

        if ($this->hasDoi()) {
            return "DOI:{$this->doi}";
        }

        return $this->url;
    }

    public function renderScholar()
    {
        return sprintf(
            'https://scholar.google.com/scholar?hl=en&%s',
            $this->hasDoi() ? sprintf('q=%s', urlencode($this->doi)) : sprintf('q=%1$s&as_ylo=%2$s&as_yhi=%2$s', urlencode(Encoder::encode($this->title)), $this->year)
        );
    }

    public function renderDblp()
    {
        if ($this->hasDoi()) {
            return sprintf('https://dblp.uni-trier.de/doi/%s', $this->doi);
        }

        return sprintf(
            'https://dblp.uni-trier.de/search?q=%s',
            urlencode(sprintf('%s year:%d:', Encoder::encode($this->title), $this->year))
        );
    }

    public function renderTitleAndEdition()
    {
        return Encoder::encode($this->title.($this->edition ? " ({$this->edition})" : ''));
    }

    public function renderClassification()
    {
        return self::classificationName($this->classification);
    }

    public function hasVolumeAndNumber()
    {
        return !empty($this->volume);
    }

    public function renderVolumeAndNumber()
    {
        if (!$this->hasVolumeAndNumber()) {
            return;
        }

        $vols = explode('/', $this->volume, 2);

        return Encoder::encode(isset($vols[1]) ? sprintf('Vol. %s, no. %s', ...$vols) : sprintf('Vol. %s', ...$vols));
    }

    public function hasSeriesAndVolume()
    {
        return !empty($this->series);
    }

    public function renderSeriesAndVolume()
    {
        if (!$this->hasSeriesAndVolume()) {
            return;
        }

        if ($this->classification == self::ARXIV) {
            return sprintf('Subject: %s', $this->series);
        }

        return Encoder::encode($this->volume ? sprintf('%s %s', $this->series, $this->volume) : $this->series);
    }

    public function toBibFile()
    {
        $data = array_filter(array_merge($this->commonBibArray(), $this->innerBibArray()));

        uksort($data, function($a, $b) {
            return array_search($a, self::BIB_ORDER) <=> array_search($b, self::BIB_ORDER);
        });

        return Bibfile::encode(static::citationType($this->classification), $this->citation_index, $data);
    }

    private function commonBibArray()
    {
        if ($this->classification == self::ARXIV) {
            return [
                'author' => $this->authors,
                'title'  => $this->title,
                'year'   => $this->year,
            ];
        }

        return [
            'author'    => $this->authors,
            'title'     => $this->title,
            'publisher' => $this->publisher,
            'series'    => $this->series,
            'year'      => $this->year,
            'doi'       => $this->doi,
        ];
    }

    private function innerBibArray()
    {
        switch ($this->classification) {
            case self::BOOK:
                return ['edition' => $this->edition, 'editor' => $this->editors, 'series' => $this->series, 'volume' => $this->volume];
            case self::IN_BOOK:
            case self::IN_PROCEEDINGS:
                if ($this->start_page) {
                    return [
                        'pages'     => $this->end_page ? "{$this->start_page}--{$this->end_page}" : "{$this->start_page}",
                        'booktitle' => $this->booktitle,
                        'editor'    => $this->editors,
                        'series'    => $this->series,
                        'volume'    => $this->volume,
                    ];
                } else {
                    return [
                        'booktitle' => $this->booktitle,
                        'editor'    => $this->editors,
                        'series'    => $this->series,
                        'volume'    => $this->volume,
                    ];
                }
            case self::ARTICLE:
                $vols = explode('/', $this->volume, 2);

                if ($this->start_page) {
                    return [
                        'pages'   => $this->end_page ? "{$this->start_page}--{$this->end_page}" : "{$this->start_page}",
                        'journal' => $this->booktitle,
                        'volume'  => $vols[0],
                        'number'  => $vols[1] ?? null,
                    ];
                } else {
                    return [
                        'journal' => $this->booktitle,
                        'volume'  => $vols[0],
                        'number'  => $vols[1] ?? null,
                    ];
                }
            case self::TECH_REPORT:
                return ['institution' => $this->institution, 'url' => $this->url];
            case self::PHD_THESIS:
                return ['school' => $this->institution, 'url' => $this->url];
            case self::HAB_THESIS:
                return ['type' => 'Habilitation Thesis', 'school' => $this->institution, 'url' => $this->url];
            case self::BSC_THESIS:
                return ['type' => '{BSc} Thesis', 'school' => $this->institution, 'url' => $this->url];
            case self::MENG_THESIS:
                return ['type' => '{MEng} Thesis', 'school' => $this->institution, 'url' => $this->url];
            case self::LECTURE_NOTES:
                return ['type' => 'Lecture Notes', 'institution' => $this->institution, 'url' => $this->url];
            case self::LECTURE_SLIDES:
                return ['type' => 'Lecture Slides', 'institution' => $this->institution, 'url' => $this->url];
            case self::TALK_SLIDES:
                return ['note' => sprintf('Presented at the %s', $this->booktitle), 'url' => $this->url];
            case self::PREPRINT:
                return ['type' => 'Submitted for publication', 'institution' => $this->institution, 'url' => $this->url];
            case self::ARXIV:
                return ['eprint' => $this->volume, 'archivePrefix' => 'arXiv', 'primaryClass' => $this->series];
        }
    }

    public static function classificationName(int $classification)
    {
        switch ($classification) {
            case self::BOOK:
                return 'Book';
            case self::IN_BOOK:
                return 'In Book';
            case self::IN_PROCEEDINGS:
                return 'In Proceedings';
            case self::ARTICLE:
                return 'Journal Article';
            case self::TECH_REPORT:
                return 'Technical Report';
            case self::PHD_THESIS:
                return 'PhD Thesis';
            case self::HAB_THESIS:
                return 'Habilitation Thesis';
            case self::BSC_THESIS:
                return 'BSc Thesis';
            case self::MENG_THESIS:
                return 'MEng Thesis';
            case self::LECTURE_NOTES:
                return 'Lecture Notes';
            case self::LECTURE_SLIDES:
                return 'Lecture Slides';
            case self::TALK_SLIDES:
                return 'Talk Slides';
            case self::PREPRINT:
                return 'Preprint';
            case self::ARXIV:
                return 'arXiv';
        }
    }

    public static function citationType(int $classification)
    {
        switch ($classification) {
            case self::BOOK:
                return 'book';
            case self::IN_BOOK:
                return 'inbook';
            case self::IN_PROCEEDINGS:
                return 'inproceedings';
            case self::ARTICLE:
                return 'article';
            case self::PHD_THESIS:
            case self::HAB_THESIS:
            case self::BSC_THESIS:
            case self::MENG_THESIS:
                return 'phdthesis';
            case self::ARXIV:
            case self::TALK_SLIDES:
                return 'misc';
            default:
                return 'techreport';
        }
    }

    public static function citationIndex(string $authors, string $year)
    {
        $names = collect(explode(' and ', Encoder::encode($authors)))->map(function ($author) {
            $last = str_replace(
                [' and', ' an', ' a'],
                ['', '', ''],
                strpos($author, ' ') === false ? $author : explode(' ', $author, 2)[1]
            );

            $parts = explode(' ', str_replace([',', '.', '-', '_', '\''], ['', '', '', '', ''], Str::ascii($last)));

            return preg_replace('/[^A-Za-z]/', '', array_pop($parts));
        })->join('-');

        $index = sprintf('%s%s', $names, substr($year, -2));

        $suffix = 'a';

        do {
            if (Publication::where('citation_index', sprintf('%s%s', $index, $suffix))->count() === 0) {
                return sprintf('%s%s', $index, $suffix);
            }
        } while ($suffix++);
    }
}
