<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('citation_index')->unique();
            $table->smallInteger('classification');
            $table->string('authors');
            $table->string('editors')->nullable();
            $table->string('title');
            $table->string('booktitle')->nullable();
            $table->string('edition')->nullable();
            $table->string('type')->nullable();
            $table->string('institution')->nullable();
            $table->string('publisher')->nullable();
            $table->string('series')->nullable();
            $table->integer('volume')->nullable();
            $table->integer('start_page')->unsigned()->nullable();
            $table->integer('end_page')->unsigned()->nullable();
            $table->string('year')->nullable();
            $table->string('doi')->nullable();
            $table->string('url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publications');
    }
}
