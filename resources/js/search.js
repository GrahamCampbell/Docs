require('./bootstrap');

import algoliasearch from 'algoliasearch/lite';
import instantsearch from 'instantsearch.js';
import { connectAutocomplete } from 'instantsearch.js/es/connectors'

const searchClient = algoliasearch(window.config.algolia.id, window.config.algolia.key);

const search = instantsearch({
  indexName: 'publications',
  searchClient
});

search.start();

// Helper for the render function
const renderIndexListItem = ({ hits }) => `
  <li>
    <ol>
      ${hits
        .map(
          hit =>
            `
            <div class="flex mb-8 pb-1">
              <div class="w-5/6 h-12">
                <li>
                  <p class="truncate"><strong>[${instantsearch.highlight({ attribute: 'classification', hit })}] <a href="/publication/${hit.citation_index}" target="_blank">${instantsearch.highlight({ attribute: 'title', hit })}</a></strong></p>
                </li>
                <li>
                  <p class="truncate">${instantsearch.highlight({ attribute: 'authors', hit })}</p>
                </li>
                <li>
                  <p class="truncate">${instantsearch.highlight({ attribute: 'subtitle', hit })}, ${instantsearch.highlight({ attribute: 'year', hit })}</p>
                </li>
              </div>
              <div class="w-1/6 h-12 text-right">
                <div class="flex flex-wrap -mb-4">
                  <div class="w-3/5 h-12">
                    <li><a href="${hit.scholar}" target="_blank">GS</a></li>
                    <li><a href="${hit.dblp}" target="_blank">DBLP</a></li>
                    <li>${hit.proxy ? '<a href="' + hit.proxy + '" target="_blank">PROXY</a>' : '<p class="text-gray-500">PROXY</p>'}</li>
                  </div>
                  <div class="w-2/5 h-12">
                  <li><a href="/publication/${hit.citation_index}.bib" target="_blank">BIB</a></li>
                  <li>${hit.attached ? '<a href="/publication/' + hit.citation_index + '.pdf" target="_blank">PDF</a>' : '<p class="text-gray-500">PDF</p>'}</li>
                  <li>${hit.link ? '<a href="' + hit.link + '" target="_blank">LINK</a>' : '<p class="text-gray-500">LINK</p>'}</li>
                  </div>
                </div>
              </div>
            </div>
            <hr class="mb-2 pb-2"/>
            `
        )
        .join('')}
    </ol>
  </li>
`;

let timerId;

// Create the render function
const renderAutocomplete = (renderOptions, isFirstRender) => {
  const { indices, currentRefinement, refine, widgetParams } = renderOptions;

  if (isFirstRender) {
    const input = document.createElement('input');
    const ul = document.createElement('ul');

    input.className = 'text-lg block w-full bg-gray-200 text-gray-700 border border-gray-300 py-3 px-4 mb-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500';
    input.placeholder = 'Search for a publication...'

    input.addEventListener('input', event => {
      clearTimeout(timerId);
      var value = event.currentTarget.value
      timerId = setTimeout(() => refine(value), 250);
    });

    const inner1 = document.createElement('div');
    inner1.className = 'w-full md:w-11/12 pr-2';
    inner1.appendChild(input);

    const inner2 = document.createElement('div');
    inner2.className = 'w-full md:w-1/12';
    inner2.innerHTML = '<input type="submit" class="text-md block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-3 mb-4 leading-relaxed" value="ADD" onclick="window.open(\'/add\');">'

    const container = document.createElement('div');
    container.className = 'flex flex-wrap';
    container.appendChild(inner1);
    container.appendChild(inner2);

    widgetParams.container.appendChild(container);
    widgetParams.container.appendChild(ul);

    input.focus();
  } else {
    widgetParams.container.querySelector('ul').innerHTML = indices
      .map(renderIndexListItem)
      .join('');
  }
};

// Create the custom widget
const customAutocomplete = connectAutocomplete(
  renderAutocomplete
);

// Instantiate the custom widget
search.addWidgets(
  [
    customAutocomplete({
      container: document.querySelector('#autocomplete')
    }),
  ],
);
