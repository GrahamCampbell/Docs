composer-install:
	@rm -rf vendor && docker run -it -w /data -v ${PWD}:/data:delegated -v ~/.composer:/root/.composer:delegated --entrypoint composer --rm registry.gitlab.com/grahamcampbell/php:7.4-base install --no-scripts

composer-update:
	@rm -rf vendor composer.lock && docker run -it -w /data -v ${PWD}:/data:delegated -v ~/.composer:/root/.composer:delegated --entrypoint composer --rm registry.gitlab.com/grahamcampbell/php:7.4-base update --no-scripts

yarn-install:
	@rm -rf node_modules && docker run -it -w /data -v ${PWD}:/data:delegated -v ~/.yarn:/usr/local/share/.cache/yarn:delegated --entrypoint yarn --rm node:14-alpine install

yarn-update:
	@rm -rf node_modules yarn.lock && docker run -it -w /data -v ${PWD}:/data:delegated -v ~/.yarn:/usr/local/share/.cache/yarn:delegated --entrypoint yarn --rm node:14-alpine install

yarn-prod:
	@docker run -it -w /data -v ${PWD}:/data:delegated -v ~/.yarn:/usr/local/share/.cache/yarn:delegated --entrypoint yarn --rm node:14-alpine prod

phpunit:
	@rm -f bootstrap/cache/*.php && docker run -it -w /data -v ${PWD}:/data:delegated --entrypoint vendor/bin/phpunit --rm registry.gitlab.com/grahamcampbell/php:7.4-base
