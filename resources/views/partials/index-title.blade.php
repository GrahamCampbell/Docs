<div class="flex flex-wrap -mx-3 my-8">
  <div class="w-full md:w-5/12 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="citation_index">
      Citation Index
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="citation_index" name="citation_index" type="text" placeholder="Blogs80a">
  </div>
  <div class="w-full md:w-7/12 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="title">
      Title
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="title" name="title" type="text" placeholder="Paper Title">
  </div>
</div>
