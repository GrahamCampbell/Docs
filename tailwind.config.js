module.exports = {
  purge: false,
  theme: {
    lineHeight: {
      none: 1,
      tight: 1.25,
      snug: 1.375,
      normal: 1.5,
      relaxed: 1.7,
      loose: 2,
    }
  },
  variants: {},
  plugins: []
}
