@extends('layouts.default')

@section('content')
<div class="relative max-w-6xl mx-auto">
  <div class="min-h-screen lg:flex-row lg:items-center lg:p-8">
    <div class="flex mb-4 pt-2 pr-2 pl-2">
      <div class="w-2/3">
        <h1 class="pb-3 text-4xl">{{ $pub->renderTitleAndEdition() }}</h1>
        <h2 class="text-2xl">{{ $pub->renderAuthors() }}</h2>
      </div>
      <div class="w-1/3 text-right">
        <h2 class="pb-2 text-4xl">{{ $pub->renderClassification() }}</h2>
        <h2 class="text-4xl">{{ $pub->year }}<h2>
      </div>
    </div>
    <hr class="p-2"/>

    <div class="flex mb-4">
      <div class="w-4/5 bg-gray-300 p-3">
        <div class="w-full h-32">
          @if($pub->hasEditors())
          <p class="pb-1 text-xl truncate">{{ $pub->renderEditors() }} (eds.)</p>
          @endif
          @if($pub->hasDescription())
          <p class="pb-1 text-xl truncate">{{ $pub->renderDescription() }}</p>
          @endif
          @if($pub->hasSeriesAndVolume())
          <p class="pb-1 text-xl truncate">{{ $pub->renderSeriesAndVolume() }}</p>
          @elseif($pub->hasVolumeAndNumber())
          <p class="pb-1 text-xl truncate">{{ $pub->renderVolumeAndNumber() }}</p>
          @endif
          @if(!$pub->hasDescription() && $pub->hasPublisher())
          <p class="pb-1 text-xl truncate">{{ $pub->renderPublisher() }}</p>
          @endif
        </div>
        @if($pub->hasLink())
        <div class="w-full">
          <p class="py-1 text-xl truncate"><a class="underline" href="{{ $pub->renderLinkUrl() }}" target="_blank">{{ $pub->renderLinkText() }}</a>@if($pub->hasProxy()) (<a class="underline" href="{{ $pub->renderProxy() }}" target="_blank">PROXY</a>)@endif</p>
        </div>
        @elseif($pub->hasProxy())
        @endif
      </div>
      <div class="w-1/5 bg-gray-400 p-3">
        <a class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white text-center font-bold py-2 mb-3 mt-1 hover:no-underline truncate" href="/publication/{{ $pub->citation_index }}.bib" target="_blank">BIB</a>
        <a class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white text-center font-bold py-2 mb-3 hover:no-underline truncate" href="{{ $pub->renderDblp() }}" target="_blank">DBLP</a>
        <a class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white text-center font-bold py-2 hover:no-underline truncate" href="{{ $pub->renderScholar() }}" target="_blank">SCHOLAR</a>
      </div>
    </div>

    @if($pub->hasPdf())
    <div class="flex mb-4">
      <div class="w-1/2">
        <a class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white text-center font-bold py-2 hover:no-underline truncate" href="/publication/{{ $pub->citation_index }}.pdf" target="_blank">View PDF</a>
      </div>
      <div class="w-1/2">
        <form action="/publication/{{ $pub->citation_index }}" method="POST">
          @csrf
          {{ method_field('DELETE') }}
          <div class="w-full">
            <input type="submit" class="text-lg block w-full bg-red-500 hover:bg-red-400 focus:outline-none focus:shadow-outline text-white font-bold py-2 truncate" value="Delete Publication" name="submit">
          </div>
        </form>
      </div>
    </div>
    @else
    <div class="flex mb-4">
      <div class="w-full">
        <form class="flex w-full" action="/publication/{{ $pub->citation_index }}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="w-1/5 bg-gray-300 p-3 text-right">
            <p class="m-2 text-lg truncate">Upload PDF File: </p>
          </div>
          <div class="w-3/5 bg-gray-300 p-3">
            @csrf
            <input type="file" class="m-2 leading-tight text-lg" name="pdf" id="pdf">
          </div>
          <div class="w-1/5 bg-gray-400 p-3">
            <input type="submit" class="text-lg block w-full bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white text-center font-bold py-2 truncate" value="Upload PDF" name="submit">
          </div>
        </form>
      </div>
    </div>
    <div class="flex mb-4">
      <div class="w-full">
        <form action="/publication/{{ $pub->citation_index }}" method="POST">
          @csrf
          {{ method_field('DELETE') }}
          <div class="w-full">
            <input type="submit" class="text-lg block w-full bg-red-500 hover:bg-red-400 focus:outline-none focus:shadow-outline text-white font-bold py-2 truncate" value="Delete Publication" name="submit">
          </div>
        </form>
      </div>
    </div>
    @endif
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ mix('/js/app.js') }}"></script>
@endsection
