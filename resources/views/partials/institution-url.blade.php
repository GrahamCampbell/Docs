<div class="flex flex-wrap -mx-3 my-8">
  <div class="w-full md:w-5/12 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="institution">
      Institution
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="institution" name="institution" type="text" placeholder="University of York">
  </div>
  <div class="w-full md:w-7/12 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="url">
      URL (Optional)
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="url" name="url" type="text" placeholder="https://etheses.whiterose.ac.uk/20255/">
  </div>
</div>
