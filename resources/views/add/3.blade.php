@extends('layouts.add')

@section('content')
<div class="relative max-w-6xl mx-auto">
  <div class="min-h-screen lg:flex-row lg:items-center lg:p-8">
    <h1 class="pb-4">[Article] Add Publication</h1>
    <h3 class="pb-1">Please enter the publication details.</h3>
    <form class="w-full" action="/add/3" method="POST">
      @csrf
      @include('partials.authors-year')
      @include('partials.index-title')
      @include('partials.journal-publisher-pages')
      @include('partials.volume-number-doi')
      @include('partials.submit')
    </form>
  </div>
</div>
@endsection
