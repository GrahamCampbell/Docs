@extends('layouts.default')

@section('content')
<div class="relative max-w-6xl mx-auto">
  <div class="min-h-screen lg:flex-row lg:items-center lg:p-8">
    <div id="autocomplete"></div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  window.config = {
    algolia: {
      id: '{{ config('scout.algolia.id') }}',
      key: '{{ config('scout.algolia.public') }}',
    }
  };
</script>
<script src="{{ mix('/js/search.js') }}"></script>
@endsection
