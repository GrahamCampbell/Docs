FROM node:14-alpine

WORKDIR /var/www/
ADD . /var/www/
RUN yarn install
RUN yarn prod

FROM registry.gitlab.com/grahamcampbell/php:7.4-base

WORKDIR /var/www/
ADD . /var/www/
RUN composer install --no-scripts --no-dev

FROM registry.gitlab.com/grahamcampbell/php:7.4-fpm

WORKDIR /var/www
COPY --from=1 /var/www/ /var/www/
RUN rm -rf /var/www/public/ /var/www/html/
COPY --from=0 /var/www/public/ /var/www/public/
RUN cp -r /var/www/public /var/www/html
RUN php artisan package:discover
WORKDIR /var/www/html
