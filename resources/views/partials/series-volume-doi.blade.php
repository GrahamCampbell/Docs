<div class="flex flex-wrap -mx-3 my-8">
  <div class="w-full md:w-1/3 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="series">
      Series (Optional)
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="series" name="series" type="text" placeholder="Lecture Notes in Computer Science">
  </div>
  <div class="w-full md:w-1/3 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="volume">
      Volume (Optional)
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="volume" name="volume" type="text" placeholder="42">
  </div>
  <div class="w-full md:w-1/3 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="doi">
      DOI (Optional)
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="doi" name="doi" type="text" placeholder="10.1007/978-3-319-75396-6_13">
  </div>
</div>
