<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(function ($router) {
                $router->get('/', 'HomeController@handleHome');
                $router->get('/publication/{file}', 'HomeController@handlePublication');
                $router->post('/publication/{file}', 'HomeController@handleUpload');
                $router->delete('/publication/{file}', 'HomeController@handleDelete');
                $router->get('/add', 'HomeController@handleAdd');
                $router->get('/add/{cat}', 'HomeController@handleAddSelect');
                $router->post('/add/{cat}', 'HomeController@handleAddPost');
                $router->post('/citation', 'HomeController@handleCitationIndex');
                $router->get('/reindex', 'HomeController@handleReindex');
            });
    }
}
