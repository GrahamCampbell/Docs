<div class="flex flex-wrap -mx-3 my-8">
  <div class="w-full md:w-3/4 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="authors">
      Authors
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="authors" name="authors" type="text" placeholder="Steve Blogs">
  </div>
  <div class="w-full md:w-1/4 px-3">
    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="year">
      Year
    </label>
    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="year" name="year" type="text" placeholder="1980">
  </div>
</div>
