<?php

namespace App\Http\Controllers;

use App\Models\Attachment;
use App\Models\Publication;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
    public function handleHome()
    {
        return view('welcome');
    }

    public function handlePublication(string $file)
    {
        @list($index, $ext) = explode('.', $file, 2);

        $pub = Publication::where('citation_index', $index)->first();

        if ($pub === null) {
            abort(404);
        }

        switch ($ext) {
            case '':
                return $this->handleShow($pub);
            case 'bib':
                return $this->handleBib($pub);
            case 'pdf':
                return $this->handlePdf($pub);
            default:
                abort(404);
        }
    }

    public function handleAdd()
    {
        return view('add');
    }

    public function handleAddSelect($cat)
    {
        return view("add.{$cat}");
    }

    public function handleCitationIndex(Request $request)
    {
        return ['citation_index' => Publication::citationIndex((string) $request->authors, (string) $request->year)];
    }

    public function handleAddPost($cat, Request $request)
    {
        switch ($cat) {
            case Publication::BOOK:
                $pub = Publication::createBook(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->authors,
                    (string) $request->editors,
                    (string) $request->year,
                    (string) $request->publisher,
                    $request->edition,
                    $request->series,
                    $request->volume,
                    $request->doi
                );
                break;
            case Publication::IN_BOOK:
                $pub = Publication::createInBook(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->booktitle,
                    (string) $request->authors,
                    (string) $request->editors,
                    (string) $request->startpage,
                    (string) $request->endpage,
                    (string) $request->year,
                    (string) $request->publisher,
                    $request->series,
                    $request->volume,
                    $request->doi
                );
                break;
            case Publication::IN_PROCEEDINGS:
                $pub = Publication::createInProceedings(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->booktitle,
                    (string) $request->authors,
                    (string) $request->editors,
                    (string) $request->startpage,
                    (string) $request->endpage,
                    (string) $request->year,
                    (string) $request->publisher,
                    $request->series,
                    $request->volume,
                    $request->doi
                );
                break;
            case Publication::ARTICLE:
                $pub = Publication::createArticle(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->journal,
                    (string) $request->authors,
                    (string) $request->startpage,
                    (string) $request->endpage,
                    (string) $request->year,
                    (string) $request->publisher,
                    (string) $request->volume,
                    $request->number,
                    $request->doi
                );
                break;
            case Publication::TECH_REPORT:
                $pub = Publication::createTechReport(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->authors,
                    (string) $request->institution,
                    (string) $request->year,
                    $request->url
                );
                break;
            case Publication::PHD_THESIS:
                $pub = Publication::createPhdThesis(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->authors,
                    (string) $request->school,
                    (string) $request->year,
                    $request->url
                );
                break;
            case Publication::HAB_THESIS:
                $pub = Publication::createHabThesis(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->authors,
                    (string) $request->school,
                    (string) $request->year,
                    $request->url
                );
                break;
            case Publication::BSC_THESIS:
                $pub = Publication::createBscThesis(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->authors,
                    (string) $request->school,
                    (string) $request->year,
                    $request->url
                );
                break;
            case Publication::MENG_THESIS:
                $pub = Publication::createMEngThesis(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->authors,
                    (string) $request->school,
                    (string) $request->year,
                    $request->url
                );
                break;
            case Publication::LECTURE_NOTES:
                $pub = Publication::createLectureNotes(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->authors,
                    (string) $request->institution,
                    (string) $request->year,
                    $request->url
                );
                break;
            case Publication::LECTURE_SLIDES:
                $pub = Publication::createLectureSlides(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->authors,
                    (string) $request->institution,
                    (string) $request->year,
                    $request->url
                );
                break;
            case Publication::TALK_SLIDES:
                $pub = Publication::createTalkSlides(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->event,
                    (string) $request->authors,
                    (string) $request->year,
                    $request->url
                );
                break;
            case Publication::PREPRINT:
                $pub = Publication::createPreprint(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->authors,
                    (string) $request->institution,
                    (string) $request->year,
                    $request->url
                );
                break;
            case Publication::ARXIV:
                $pub = Publication::createArxiv(
                    (string) $request->citation_index,
                    (string) $request->title,
                    (string) $request->authors,
                    (string) $request->year,
                    (string) $request->eprint,
                    (string) $request->clazz
                );
                break;
        }

        return redirect("/publication/{$pub->citation_index}");
    }

    public function handleUpload(string $index, Request $request)
    {
        $pub = Publication::where('citation_index', $index)->first();

        if ($pub === null) {
            abort(404);
        }

        if (!$request->hasFile('pdf') || !$request->pdf->isValid()) {
            abort(400);
        }

        $pdf = Attachment::forceCreate([
            'type'           => Attachment::PDF,
            'filesize'       => filesize($request->pdf->path()),
            'sha1'           => sha1_file($request->pdf->path()),
            'publication_id' => $pub->id,
        ]);

        $request->pdf->storeAs('local', "{$pdf->id}.pdf");

        Publication::find($pub->id)->save();

        return redirect("/publication/{$pub->citation_index}");
    }

    public function handleDelete(string $index)
    {
        $pub = Publication::where('citation_index', $index)->first();

        if ($pub === null) {
            abort(404);
        }

        foreach ($pub->attachments as $attachment) {
            unlink($attachment->filePath());
            $attachment->delete();
        }

        $pub->delete();

        return redirect('/');
    }

    protected function handleShow(Publication $pub)
    {
        return view('publication', ['pub' => $pub]);
    }

    protected function handleBib(Publication $pub)
    {
        return response($pub->toBibFile())
            ->header('Content-Type', 'text/plain')
            ->header('Content-Disposition', 'inline');
    }

    protected function handlePdf(Publication $pub)
    {
        $pdf = $pub->attachments()->where('type', Attachment::PDF)->first();

        if ($pdf === null) {
            abort(404);
        }

        return response(file_get_contents($pdf->filePath()))
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', 'inline');
    }

    public function handleReindex()
    {
        Publication::removeAllFromSearch();
        Publication::makeAllSearchable();

        return ['message' => 'All publications have been reindexed.'];
    }
}
